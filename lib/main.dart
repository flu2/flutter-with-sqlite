import 'package:flutter/material.dart';
import 'dog.dart';
import 'dog_dao.dart';

void main() async {
  var fido = Dog(id: 0, name: 'Fido', age: 35);
  var pido = Dog(id: 1, name: 'Pido', age: 25);
  await DogDAO.insertDog(fido);
  await DogDAO.insertDog(pido);

  print(await DogDAO.dogs());

  fido = Dog(
    id: fido.id,
    name: fido.name,
    age: fido.age + 7,
  );

  await DogDAO.updateDog(fido);
  print(await DogDAO.dogs());

  await DogDAO.deleteDog(0);
  print(await DogDAO.dogs());
}
